package org.example.BookingManagement.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Booking {
    @Id
    private String id;

    private String matchId;
    private String ticketId;
    private String customerId;

    public Booking(String matchId, String ticketId, String customerId) {
        this.matchId = matchId;
        this.ticketId = ticketId;
        this.customerId = customerId;
    }

    public Booking() {
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "matchId='" + matchId + '\'' +
                ", ticketId='" + ticketId + '\'' +
                ", customerId='" + customerId + '\'' +
                '}';
    }
}
