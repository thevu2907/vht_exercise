package org.example.BookingManagement.controller;

import org.example.BookingManagement.entities.Booking;
import org.example.BookingManagement.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/book", produces = { "application/json" })
public class BookingController {
    @Autowired
    private BookingRepository bookingRepository;

    @PostMapping("/add")
    private Booking addCustomerApi(@RequestBody Booking match){
        return bookingRepository.save(match);
    }

    @GetMapping("/get/available")
    private List<Booking> getAllCustomersApi(){
        return bookingRepository.findAll();
    }
}
