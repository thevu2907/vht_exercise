package org.example.MatchManagement.controller;

import org.example.MatchManagement.entities.Match;
import org.example.MatchManagement.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/match", produces = { "application/json" })
public class MatchController {

    @Autowired
    private MatchRepository matchRepository;

    @PostMapping("/add")
    private Match addCustomerApi(@RequestBody Match match){
        return matchRepository.save(match);
    }

    @GetMapping("/get/{matchId}")
    private Optional<Match> getCustomerByIdApi(@PathVariable String matchId){
        return matchRepository.findById(matchId);
    }

//    @GetMapping("/ticket/changeStatus")

}
