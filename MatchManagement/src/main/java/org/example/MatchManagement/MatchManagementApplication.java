package org.example.MatchManagement;

import org.example.MatchManagement.entities.Match;
import org.example.MatchManagement.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchManagementApplication implements CommandLineRunner {

	@Autowired
	private MatchRepository matchRepository;

	public static void main(String[] args) {
		SpringApplication.run(MatchManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if(matchRepository.findAll().isEmpty()){
			matchRepository.save(new Match(0, null));
			matchRepository.save(new Match(0, null));
			matchRepository.save(new Match(0, null));
			matchRepository.save(new Match(0, null));
		}

		for(Match match : matchRepository.findAll()){
			System.out.println(match.toString());
		}
	}
}
