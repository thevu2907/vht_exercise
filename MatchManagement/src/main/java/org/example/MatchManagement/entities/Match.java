package org.example.MatchManagement.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Document
public class Match {
    @Id
    private String id;
    @Field
    private int numberOfTicket;
    @Field
    private List<Ticket> ticketList;

    public Match(int numberOfTicket, List<Ticket> ticketList) {
        this.numberOfTicket = numberOfTicket;
        this.ticketList = ticketList;
    }

    public Match() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumberOfTicket() {
        return numberOfTicket;
    }

    public void setNumberOfTicket(int numberOfTicket) {
        this.numberOfTicket = numberOfTicket;
    }

    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id='" + id + '\'' +
                ", numberOfTicket=" + numberOfTicket +
                ", ticketList=" + ticketList +
                '}';
    }
}
