package org.example.CustomerManagement.controller;

import org.example.CustomerManagement.entities.Customer;
import org.example.CustomerManagement.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/customers", produces = { "application/json" })
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @PostMapping("/add")
    private Customer addCustomerApi(@RequestBody Customer customer){
        return customerRepository.save(customer);
    }

    @GetMapping("/get/{customerId}")
    private Optional<Customer> getCustomerByIdApi(@PathVariable String customerId){
        return customerRepository.findById(customerId);
    }

    @GetMapping("/getAll")
    private List<Customer> getAllCustomersApi(){
        return customerRepository.findAll();
    }
}
