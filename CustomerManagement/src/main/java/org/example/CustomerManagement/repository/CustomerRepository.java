package org.example.CustomerManagement.repository;

import org.example.CustomerManagement.entities.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
}
