package org.example.CustomerManagement;

import org.example.CustomerManagement.entities.Customer;
import org.example.CustomerManagement.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerManagementApplication implements CommandLineRunner {


	@Autowired
	private CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(CustomerManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if(customerRepository.findAll().isEmpty()){
			customerRepository.save(new Customer("Vu Xuan The"));
			customerRepository.save(new Customer("Vu Xuan The1"));
			customerRepository.save(new Customer("Vu Xuan The2"));
		}

		for(Customer customer : customerRepository.findAll()){
			System.out.println(customer.toString());
		}
	}
}
